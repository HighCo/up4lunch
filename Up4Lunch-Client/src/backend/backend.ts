import {ObjectId} from './objectid'
//var url = "http://localhost:3003/server/";
var url = "http://up4lunch.com/server/";

export var user = {
    _id:"",
    name:"",
    email:"",
    joinedDayIndex:0
}

var createUserPromise = Promise.resolve();

export function initUser()
{
    var userString = localStorage.getItem("up4lunch-user");
    if(userString)
    {
        user = JSON.parse(userString)
        return user.name != "";
    }
    else
    {
        //createUserPromise = createUser();
        return false;
    }
}

function get(path:string, body?)  { return request("get", path, body) }
function post(path:string, body?) { return request("post", path, body) }
function put(path:string, body?)  { return request("put", path, body) }
function del(path:string, body?)  { return request("delete", path, body) }
async function request(method:string, path:string, body?)
{
    var request = await fetch(url+path, {method, body:JSON.stringify(body)});
    return request.json();
}

export async function signup(name:string, email:string)
{
    try
    {
        user._id = new ObjectId().toString()
        user.name = name;
        user.email = email;
        var result = await post("user", user)
        localStorage.setItem("up4lunch-user", JSON.stringify(user));
        if(result != "ok") alert(result)
    }
    catch(ex)
    {
        alert("Error creating user "+ex);
    }
}

export async function join(dayIndex:number)
{
    user.joinedDayIndex = dayIndex;
    localStorage.setItem("up4lunch-user", JSON.stringify(user));
    var result = await put("join/" + user._id);
    alert(result)
}
