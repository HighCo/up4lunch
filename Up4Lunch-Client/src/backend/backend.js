var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
export var TrackingStep;
(function (TrackingStep) {
    /*0*/ TrackingStep[TrackingStep["Welcome"] = 0] = "Welcome";
    /*1*/ TrackingStep[TrackingStep["Goals"] = 1] = "Goals";
    /*2*/ TrackingStep[TrackingStep["AskToSignup"] = 2] = "AskToSignup";
    /*3*/ TrackingStep[TrackingStep["StartSignup"] = 3] = "StartSignup";
    /*4*/ TrackingStep[TrackingStep["FirstSession"] = 4] = "FirstSession";
    /*5*/ TrackingStep[TrackingStep["Play"] = 5] = "Play";
    /*6*/ TrackingStep[TrackingStep["Completed"] = 6] = "Completed";
    /*7*/ TrackingStep[TrackingStep["SetReminder"] = 7] = "SetReminder";
})(TrackingStep || (TrackingStep = {}));
export var SignupMethod;
(function (SignupMethod) {
    SignupMethod[SignupMethod["EMail"] = 0] = "EMail";
    SignupMethod[SignupMethod["Facebook"] = 1] = "Facebook";
})(SignupMethod || (SignupMethod = {}));
export var user = {
    id: "",
    name: "",
    group: 0,
    step: 0,
    reminderHour: 22,
    reminderMinute: 0,
    goals: [],
    progress: {}
};
var createUserPromise = Promise.resolve();
export function initUser() {
    var userString = localStorage.getItem("user");
    if (userString) {
        user = Object.assign(user, JSON.parse(userString));
        return user.name != "";
    }
    else {
        createUserPromise = createUser();
        return false;
    }
}
// ----------- Firestore -----------
firebase.initializeApp({
    apiKey: "AIzaSyCjmyd_qUHgmL72HvlAo-zGCmOFeAb8Qek",
    authDomain: "touch-e8708.firebaseapp.com",
    databaseURL: "https://touch-e8708.firebaseio.com",
    projectId: "touch-e8708",
    storageBucket: "touch-e8708.appspot.com",
    messagingSenderId: "628123986127",
});
var db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });
var trackingCollection = db.collection("tracking");
var userCollection = db.collection("user");
var auth = firebase.auth();
export function track(step, params) {
    if (params === void 0) { params = null; }
    return __awaiter(this, void 0, void 0, function () {
        var text;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, createUserPromise];
                case 1:
                    _a.sent();
                    text = TrackingStep[step];
                    if (typeof facebookConnectPlugin != "undefined")
                        facebookConnectPlugin.logEvent(text, params);
                    console.log("[" + step + "] " + text);
                    if (step > user.step)
                        updateUser({ step: step });
                    trackingCollection.add({ step: step, group: user.group, userName: user.name, text: text, time: new Date(), params: params });
                    return [2 /*return*/];
            }
        });
    });
}
export function createUser() {
    return __awaiter(this, void 0, void 0, function () {
        var userCredentials, locationRequest, location, ex_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 4, , 5]);
                    return [4 /*yield*/, auth.signInAnonymously()];
                case 1:
                    userCredentials = _a.sent();
                    user.id = userCredentials.uid;
                    localStorage.setItem("user", JSON.stringify(user));
                    return [4 /*yield*/, fetch("http://api.hostip.info/get_json.php")];
                case 2:
                    locationRequest = _a.sent();
                    return [4 /*yield*/, locationRequest.json()];
                case 3:
                    location = _a.sent();
                    userCollection.doc(user.id).set({ country: location.country_name, city: location.city, created: new Date(), step: 0 });
                    return [3 /*break*/, 5];
                case 4:
                    ex_1 = _a.sent();
                    // TODO: Retry creating user
                    alert("Error creating user " + JSON.stringify(ex_1));
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    });
}
export function updateUser(data, remoteData) {
    if (remoteData === void 0) { remoteData = null; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    user = Object.assign(user, data);
                    return [4 /*yield*/, createUserPromise];
                case 1:
                    _a.sent();
                    user = Object.assign(user, data);
                    localStorage.setItem("user", JSON.stringify(user));
                    userCollection.doc(user.id).set(remoteData ? remoteData : data, { merge: true });
                    return [2 /*return*/];
            }
        });
    });
}
export function signupUserWithEMail(email, password) {
    return __awaiter(this, void 0, void 0, function () {
        var credential, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    credential = firebase.auth.EmailAuthProvider.credential(email, password);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, auth.currentUser.linkAndRetrieveDataWithCredential(credential)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    e_1 = _a.sent();
                    if (e_1.code == "auth/email-already-in-use") {
                        alert("An account with this email address already exists");
                    }
                    else {
                        alert("EMail signup failed. " + e_1);
                    }
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    });
}
function openUser(id) {
    return __awaiter(this, void 0, void 0, function () {
        var remoteUser;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userCollection.doc(id).get()];
                case 1:
                    remoteUser = _a.sent();
                    user.id = remoteUser.id;
                    user.name = remoteUser.name;
                    user.group = remoteUser.group;
                    user.step = remoteUser.step;
                    user.reminderHour = remoteUser.reminderHour;
                    user.reminderMinute = remoteUser.reminderMinute;
                    user.goals = remoteUser.goals;
                    return [2 /*return*/];
            }
        });
    });
}
export function signupUserWithFacebook(accessToken) {
    return __awaiter(this, void 0, void 0, function () {
        var credential, e_2, remote, e_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    credential = firebase.auth.FacebookAuthProvider.credential(accessToken);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 9]);
                    return [4 /*yield*/, auth.currentUser.linkAndRetrieveDataWithCredential(credential)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 9];
                case 3:
                    e_2 = _a.sent();
                    _a.label = 4;
                case 4:
                    _a.trys.push([4, 7, , 8]);
                    return [4 /*yield*/, auth.signInAndRetrieveDataWithCredential(credential)];
                case 5:
                    remote = _a.sent();
                    return [4 /*yield*/, openUser(remote.user.uid)];
                case 6:
                    _a.sent();
                    return [3 /*break*/, 8];
                case 7:
                    e_3 = _a.sent();
                    alert("Facebook signup failed. " + e_3);
                    return [3 /*break*/, 8];
                case 8: return [3 /*break*/, 9];
                case 9: return [2 /*return*/];
            }
        });
    });
}
