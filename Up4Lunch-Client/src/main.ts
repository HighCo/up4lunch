import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import {initUser, user} from "./backend/backend"
import './base'

Vue.config.productionTip = false
if(!initUser())
    router.push("/signup")


Vue.filter('formatTime', value => {
    var s = value / 1000;
    var minutes = Math.floor(s / 60);
    var seconds = Math.floor(s % 60);
    return minutes + ":" + (seconds < 10 ? "0" + seconds : seconds);
})

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App },
})

document.addEventListener('deviceready', () =>
{
    setTimeout(function()
    {
        (<any>navigator).splashscreen.hide();
    }, 500);
}, false);

