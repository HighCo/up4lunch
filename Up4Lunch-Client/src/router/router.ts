import Vue from 'vue'
import Router, {RouterOptions} from 'vue-router'
import home from '../pages/Home.vue'
import signup from '../pages/Signup.vue'

Vue.use(Router);

const routerOptions: RouterOptions =
    {
        routes: [
            { path: '/signup', component: signup },
            { path: '/', component: home },
        ],
    }

export default new Router(
    routerOptions,
)
