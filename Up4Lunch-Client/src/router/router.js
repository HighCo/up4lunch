import Vue from 'vue';
import Router from 'vue-router';
import onboarding from '../pages/onboarding/Onboarding.vue';
import session from '../pages/session/Session.vue';
import reminder from '../pages/reminder/Reminder.vue';
Vue.use(Router);
var routerOptions = {
    routes: [
        { path: '/', component: onboarding },
        { path: '/sessions/1', component: session },
        { path: '/reminder', component: reminder },
    ],
};
export default new Router(routerOptions);
