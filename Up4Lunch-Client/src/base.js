Array.prototype.first = function (condition) {
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (condition(item))
            return item;
    }
    return null;
};
Array.prototype.firstIndex = function (condition) {
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (condition(item))
            return i;
    }
    return -1;
};
