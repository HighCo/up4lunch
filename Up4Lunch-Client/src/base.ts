interface Array<T>
{
    first(condition:(value:T)=>boolean):T;
    firstIndex(condition:(value:T)=>boolean):number;
}

Array.prototype.first = function(condition:(value:any)=>boolean)
{
    for(var i=0; i<this.length; i++)
    {
        var item = this[i];
        if(condition(item))
            return item;
    }
    return null;
}

Array.prototype.firstIndex = function(condition:(value:any)=>boolean)
{
    for(var i=0; i<this.length; i++)
    {
        var item = this[i];
        if(condition(item))
            return i;
    }
    return -1;
}
