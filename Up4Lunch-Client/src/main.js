// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import store from './store/store';
import { initUser } from "./backend/backend";
import 'vue-smooth-picker/dist/css/style.css';
import SmoothPicker from 'vue-smooth-picker';
import './base';
Vue.use(SmoothPicker);
Vue.config.productionTip = false;
if (initUser())
    router.push("/sessions/1");
if (new Date() >= new Date(2018, 5, 17)) {
    alert("Update required. To continue to use this app, please update it to the latest version.");
    throw new Error();
}
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router: router, store: store,
    template: '<App/>',
    components: { App: App },
});
document.addEventListener('deviceready', function () {
    setTimeout(function () {
        navigator.splashscreen.hide();
    }, 500);
}, false);
