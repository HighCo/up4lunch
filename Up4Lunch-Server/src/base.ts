interface Array<T> 
{
    clear():void;
    add(value:T):T;
    ensure(value:T):T;
    any(condition:(value:T)=>boolean):boolean;
    all(condition:(value:T)=>boolean):boolean;
    first(condition:(value:T)=>boolean):T;
    last(condition:(value:T)=>boolean):T;
    contains(item:T):boolean;
    remove(item:T):void;
    removeAt(index:number):void;
    removeWhere(condition:(value:T)=>boolean):void;
    min(selector?:(value:T)=>number|string, theshold?:number|string):T;
    max(selector?:(value:T)=>number|string, theshold?:number|string):T;
    minValue<T2>(selector?:(value:T)=>T2, theshold?:T2):T2;
    maxValue<T2>(selector?:(value:T)=>T2, theshold?:T2):T2;
    minIndex(selector?:(value:T)=>number|string):number;
    maxIndex(selector?:(value:T)=>number|string):number;
    sum(selector?:(value:T)=>number):number;
    random():T;
    insertAfter(position:T, value:T):void;
    insertAt(index:number, value:T):void;
    distinct():T[];
    distinct<Target>(selector?:(value:T)=>Target):Target[];
	count(condition:(value:T)=>boolean):number;
	shuffle():Array<T>;
	orderBy(selector:(value:T)=>number):Array<T>;
	orderByDescending(selector:(value:T)=>number):Array<T>;
	set(values:T[]);
	pushRange(values:T[]);
	pushIfDistinct(item:T);
	equals(value:T[]):boolean;
}
Array.prototype.clear = function()
{
	while(this.length > 0)
    	this.pop();
}
Array.prototype.add = function<T>(value:T)
{
	this.push(value);
	return value;
}
Array.prototype.ensure = function<T>(value:T)
{
	if(!this.contains(value))
		this.push(value);
	return value;
}
Array.prototype.any = function(condition:(value:any)=>boolean)
{
	for(var i=0; i<this.length; i++)
		if(condition(this[i]))
			return true;
	return false;
}
Array.prototype.all = function(condition:(value:any)=>boolean)
{
	for(var i=0; i<this.length; i++)
		if(!condition(this[i]))
			return false;
	return true;
}
Array.prototype.first = function(condition:(value:any)=>boolean)
{
	for(var i=0; i<this.length; i++)
	{
	    var item = this[i];
		if(condition(item))
			return item;
	}
	return null;
}
Array.prototype.last = function(condition:(value:any)=>boolean)
{
	for(var i=this.length-1; i>=0; i--)
	{
	    var item = this[i];
		if(condition(item))
			return item;
	}
	return null;
}
Array.prototype.contains = function(item:any)
{
	return this.indexOf(item) != -1;
}
Array.prototype.pushIfDistinct = function(item:any)
{
	if(this.indexOf(item) != -1)
		this.push(item);
}
Array.prototype.removeWhere = function(condition:(value:any)=>boolean)
{
	for(var i=this.length-1; i>=0; i--)
	{
		var item = this[i];
		if(condition(item))
			this.splice(i,1);
	}
}
Array.prototype.remove = function<T>(item:T)
{
	var index = this.indexOf(item);
	if(index > -1) this.splice(index,1);
}
Array.prototype.removeAt = function(index:number)
{
	this.splice(index,1);
}
Array.prototype.minIndex = function<T>(selector?:(value:T)=>number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			var minIndex = 0;
			var minValueAfterSelector = selector(this[0]);
			for(var i=1; i<this.length; i++)
			{
				var valueAfterSelector = selector(this[i]);
				if(valueAfterSelector < minValueAfterSelector)
				{
					minValueAfterSelector = valueAfterSelector;
					minIndex = i;
				}
			}
			return minIndex;
		}
		else
		{
			var minIndex = 0;
			var minValue = this[0];
			for(var i=1; i<this.length; i++)
			{
				var value = this[i];
				if(value < minValue)
				{
					minValue = value;
					minIndex = i;
				}
			}
			return minIndex;
		}
	}
	else
		return -1;
}
Array.prototype.maxIndex = function<T>(selector?:(value:T)=>number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			var maxIndex = 0;
			var maxValueAfterSelector = selector(this[0]);
			for(var i=1; i<this.length; i++)
			{
				var valueAfterSelector = selector(this[i]);
				if(valueAfterSelector > maxValueAfterSelector)
				{
					maxValueAfterSelector = valueAfterSelector;
					maxIndex = i;
				}
			}
			return maxIndex;
		}
		else
		{
			var maxIndex = 0;
			var minValue = this[0];
			for(var i=1; i<this.length; i++)
			{
				var value = this[i];
				if(value > minValue)
				{
					minValue = value;
					maxIndex = i;
				}
			}
			return maxIndex;
		}
	}
	else
		return -1;
}
Array.prototype.min = function<T>(selector?:(value:T)=>number|string, theshold?:number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			if(theshold !== void 0)
			{
				var minValue = null;
				var minValueAfterSelector = theshold;
				var start = 0;
			}
			else
			{
				var minValue = this[0];
				var minValueAfterSelector = selector(minValue);
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				var valueAfterSelector = selector(value);
				if(valueAfterSelector < minValueAfterSelector)
				{
					minValueAfterSelector = valueAfterSelector;
					minValue = value;
				}
			}
			return minValue;
		}
		else
		{
			if(theshold !== void 0)
			{
				var minValue = <any>theshold;
				var start = 0;
			}
			else
			{
				var minValue = this[0];
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				if(value < minValue)
				{
					minValue = value;
				}
			}
			return minValue < theshold ? minValue : null;
		}
	}
	else
		return selector ? null : theshold;
}
Array.prototype.minValue = function<T>(selector?:(value:T)=>number|string, theshold?:number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			if(theshold !== void 0)
			{
				var minValue = null;
				var minValueAfterSelector = theshold;
				var start = 0;
			}
			else
			{
				var minValue = this[0];
				var minValueAfterSelector = selector(minValue);
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				var valueAfterSelector = selector(value);
				if(valueAfterSelector < minValueAfterSelector)
				{
					minValueAfterSelector = valueAfterSelector;
					minValue = value;
				}
			}
			return minValueAfterSelector;
		}
		else
		{
			if(theshold !== void 0)
			{
				var minValue = <any>theshold;
				var start = 0;
			}
			else
			{
				var minValue = this[0];
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				if(value < minValue)
				{
					minValue = value;
				}
			}
			return minValue < theshold ? minValue : null;
		}
	}
	else
		return theshold;
}

Array.prototype.max = function<T>(selector?:(value:T)=>number|string, theshold?:number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			if(theshold !== void 0)
			{
				var maxValue = null;
				var maxValueAfterSelector = theshold;
				var start = 0;
			}
			else
			{
				var maxValue = this[0];
				var maxValueAfterSelector = selector(maxValue);
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				var valueAfterSelector = selector(value);
				if(valueAfterSelector > maxValueAfterSelector)
				{
					maxValueAfterSelector = valueAfterSelector;
					maxValue = value;
				}
			}
			return maxValue;
		}
		else
		{
			if(theshold !== void 0)
			{
				var maxValue = <any>theshold;
				var start = 0;
			}
			else
			{
				var maxValue = this[0];
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				if(value > maxValue)
				{
					maxValue = value;
				}
			}
			return maxValue;
		}
	}
	else
		return selector ? null : theshold;
}

Array.prototype.maxValue = function<T>(selector?:(value:T)=>number|string, theshold?:number|string)
{
	if(this.length > 0)
	{
		if(selector)
		{
			if(theshold !== void 0)
			{
				var maxValue = null;
				var maxValueAfterSelector = theshold;
				var start = 0;
			}
			else
			{
				var maxValue = this[0];
				var maxValueAfterSelector = selector(maxValue);
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				var valueAfterSelector = selector(value);
				if(valueAfterSelector > maxValueAfterSelector)
				{
					maxValueAfterSelector = valueAfterSelector;
					maxValue = value;
				}
			}
			return maxValueAfterSelector;
		}
		else
		{
			if(theshold !== void 0)
			{
				var maxValue = <any>theshold;
				var start = 0;
			}
			else
			{
				var maxValue = this[0];
				var start = 1;
			}
			for(var i=start; i<this.length; i++)
			{
				var value = this[i];
				if(value > maxValue)
				{
					maxValue = value;
				}
			}
			return maxValue;
		}
	}
	else
		return theshold;
}

Array.prototype.sum = function<T>(selector?:(value:T)=>number)
{
	if(this.length > 0)
	{
		if(selector)
		{
			var result = 0;
			for(var i=0; i<this.length; i++)
				result += selector(this[i]);
			return result;

		}
		else
		{
			var result = 0;
			for(var i=0; i<this.length; i++)
				result += this[i];
			return result;
		}
	}
	else
		return 0;
}

Array.prototype.insertAfter = function<T>(position:T, value:T)
{
	var pos:number = this.indexOf(position);
	if(pos > -1) this.splice(pos+1, 0, value);
	else         this.push(value);
}

Array.prototype.insertAt = function<T>(index:number, value:T)
{
	this.splice(index, 0, value);
}

Array.prototype.distinct = function<T>(selector?:(value:T)=>any)
{
	if(selector)
	{
		var existingValues = {}, result = [];
		for(var i = 0; i < this.length; i++)
		{
			var value = selector(this[i]);
			if(value === void 0 || existingValues[value] == true) continue;
			existingValues[value] = true;
			result.push(value);
		}
		return result;
	}
	else
	{
		var existingValues = {}, result = [];
		for(var i = 0; i < this.length; i++)
		{
			var value = this[i];
			if(value === void 0 || existingValues[value] == true) continue;
			existingValues[value] = true;
			result.push(value);
		}
		return result;
	}
}
Array.prototype.count = function(condition:(value:any)=>boolean)
{
	var count = 0;
	for(var item of this)
		if(condition(item))
			count++;
	return count;
}
Array.prototype.shuffle = function()
{
	for (var i = this.length - 1; i > 0; i--) 
	{
        var j = Math.floor(Math.random() * (i + 1));
        var temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
}
Array.prototype.orderBy = function<T>(selector:(value:T)=>number):Array<T>
{
	return this.sort((a,b) => selector(a) - selector(b));
}
Array.prototype.orderByDescending = function<T>(selector:(value:T)=>number):Array<T>
{
	return this.sort((a,b) => selector(b) - selector(a));
}
Array.prototype.pushRange = function<T>(values:T[])
{
	for(var item of values)
		this.push(item);
}
Array.prototype.set = function<T>(values:T[])
{
	while(this.length > 0)
    	this.pop();
	for(var item of values)
		this.push(item);
}
Array.prototype.random = function()
{
	if(this.length > 0)
		return this[Math.floor(Math.random() * this.length)];
	else
		return -1;
}
Array.prototype.equals = function<T>(otherArray:T[]):boolean
{
	if(!otherArray || this.length != otherArray.length)
		return false;
	for(var i=0; i<this.length; i++)
		if(this[i] != otherArray[i])
			return false;
	return true;
}
interface String
{
    startsWith(value:string):boolean;
    startsWithAny(values:string[]):boolean;
    contains(value:string):boolean;
    containsAny(values:string[]):boolean;
    endsWith(value:string):boolean;
    trimLeft(chars:string):string;
    trimRight(chars:string):string;
    escapeHtml():string;
}

String.prototype.startsWith = function(value:string)
{
    return this.substring(0,value.length) == value;
}

String.prototype.startsWithAny = function(values:string[])
{
    for(var i=0; i<values.length; i++)
    {
        var value = values[i];
        if(this.substring(0,value.length) == value)
            return true;
    }
    return false;
}

String.prototype.endsWith = function(value:string)
{
    return this.substring(this.length-value.length) == value; 
}

String.prototype.contains = function(value:string)
{
    return this.indexOf(value) != -1;
}

String.prototype.containsAny = function(values:string[])
{
    for(var i=0; i<values.length; i++)
    {
        var value = values[i];
        if(this.indexOf(value) != -1)
            return true;
    }
    return false;
}

String.prototype.trimLeft = function(chars = "\s") 
{
	return this.replace(new RegExp("^[" + chars + "]+"), "");
}

String.prototype.trimRight = function(chars = "\s") 
{
	return this.replace(new RegExp("[" + chars + "]+$"), "");
}

var escapeHtmlEntityMap = 
{
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
}

String.prototype.escapeHtml = function() 
{
	return this.replace(/[&<>"'\/]/g, s => escapeHtmlEntityMap[s]);
}

Object.defineProperty(Array, "x", {
	get:function():number{ return this[0]; },
	set:function(value:number){ this[0]=value; },
	configurable:true,
	enumerable:false
});

Object.defineProperty(Array, "y", {
	get:function():number{ return this[1]; },
	set:function(value:number){ this[1]=value; },
	configurable:true,
	enumerable:false
});

class Vector2
{
	static get minValue() {return new Vector2(Number.MIN_VALUE, Number.MIN_VALUE);}
	static get maxValue() {return new Vector2(Number.MAX_VALUE, Number.MAX_VALUE);}
	
	x:number;
	y:number;
	constructor(x:number, y:number)
	constructor(value:{x:number, y:number})
	constructor(x:number|{x:number, y:number}, y?:number)
	{
		if(typeof x === "number")
		{
			this.x = x;
			this.y = y;
		}
		else
		{
			this.x = x.x;
			this.y = x.y;
		}
	}
	clone() { return new Vector2(this.x, this.y) }
	add(value:Vector2) { return new Vector2(this.x + value.x, this.y + value.y); }
	increase(value:Vector2) { this.x += value.x; this.y += value.y; }
	subtract(value:Vector2) { return new Vector2(this.x - value.x, this.y - value.y); }
	
	multiply(value:number|Vector2)
	{ 
		if(typeof value === "number")
			return new Vector2(this.x * value, this.y * value);
		else
			return new Vector2(this.x * value.x, this.y * value.y);
	}
	
	divide(value:number|Vector2)
	{ 
		if(typeof value === "number")
			return new Vector2(this.x / value, this.y / value);
		else
			return new Vector2(this.x / value.x, this.y / value.y);
	}
	
	equal(value:Vector2) { return this.x == value.x && this.y == value.y; }
	unequal(value:Vector2) { return this.x != value.x || this.y != value.y; }
	greaterOrEqual(value:Vector2) { return this.x >= value.x && this.y >= value.y; }
	greater(value:Vector2) { return this.x > value.x && this.y > value.y; }
	lessOrEqual(value:Vector2) { return this.x <= value.x && this.y <= value.y; }
	less(value:Vector2) { return this.x < value.x && this.y < value.y; }
	get length() { return Math.sqrt(this.x * this.x + this.y * this.y); }
	get inverse() { return new Vector2(-this.x, -this.y); }
	distanceTo(value:Vector2)
	{ 
		var x = this.x - value.x;
		var y = this.y - value.y;
		return Math.sqrt(x*x + y*y); 
	}
	
	transform(matrix:Matrix2D) 
	{
		var m = matrix.matrix;
		return new Vector2(
			m[0] * this.x + m[2] * this.y + m[4],
			m[1] * this.x + m[3] * this.y + m[5]
		)
	}
	
	relativeTo(matrix:Matrix2D)
	{
		var a = matrix.matrix;
		var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
		
		var det = aa * ad - ab * ac;
		if(!det) return;
		det = 1.0 / det;
		
		return new Vector2(
			( ad * det) * this.x + (-ac * det) * this.y + ((ac * aty - ad * atx) * det),
			(-ab * det) * this.x + ( aa * det) * this.y + ((ab * atx - aa * aty) * det)
		)
	}
	
	static distanceBetween(a:Vector2, b:Vector2)
	{
		var x = a.x - b.x;
		var y = a.y - b.y;
		return Math.sqrt(x*x + y*y);
	}
	
	static angleBetween(a:Vector2, b:Vector2)
	{
		return Math.atan2(b.y - a.y, b.x - a.x);
	}
	
	static lerp(a:Vector2, b:Vector2, value:number)
	{
		var ivalue = 1 - value;
		return new Vector2(a.x * ivalue + b.x * value, a.y * ivalue + b.y * value);
	}
	
	isInsidePoygon(poly:Vector2[])
	{
		for(var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
			((poly[i].y <= this.y && this.y < poly[j].y) || (poly[j].y <= this.y && this.y < poly[i].y))
			&& (this.x < (poly[j].x - poly[i].x) * (this.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
			&& (c = !c);
		return c;
	}
	
	normalize()
	{
		var length = Math.sqrt(this.x * this.x + this.y * this.y)
		return length != 0 ? new Vector2(this.x / length, this.y / length) : new Vector2(0, 0);
	}
}

interface Number
{
    limitToRange(min:number, max:number):number;
    limitToAbove(min:number):number;
    limitToBelow(max:number):number;
    format(format:string):string;
}

interface Math
{
    limitToRange(value:number, min:number, max:number):number;
}

Math.limitToRange = function(value:number, min:number, max:number)
{
    if(value<min) return min;
    if(value>max) return max;
                  return value;	
}

Number.prototype.limitToRange = function(min:number, max:number)
{
    if(this<min) return min;
    if(this>max) return max;
                 return this;
}

Number.prototype.limitToAbove = function(min:number)
{
    if(this<min) return min;
                 return this;
}

Number.prototype.limitToBelow = function(max:number)
{
    if(this>max) return max;
                 return this;
}

Number.prototype.format = function(format:string)
{
	var m:any = format;
	var v:any = this;
    if(!m || isNaN(+v))
	{
        return v; //return as it is.
    }
    //convert any string to number according to formation sign.
    var v = <any>(m.charAt(0) == '-' ? -v : +v);
    var isNegative = v < 0 ? v = -v : 0; //process only abs(), and turn on flag.
    
    //search for separator for grp & decimal, anything not digit, not +/- sign, not #.
    var result = m.match(/[^\d\-\+#]/g);
    var Decimal = (result && result[result.length - 1]) || '.'; //treat the right most symbol as decimal 
    var Group = (result && result[1] && result[0]) || ',';  //treat the left most symbol as group separator
    
    //split the decimal for the format string if any.
    var m = m.split(Decimal);
    //Fix the decimal first, toFixed will auto fill trailing zero.
    v = v.toFixed(m[1] && m[1].length);
    v = +(v) + ''; //convert number to string to trim off *all* trailing decimal zero(es)

    //fill back any trailing zero according to format
    var pos_trail_zero = m[1] && m[1].lastIndexOf('0'); //look for last zero in format
    var part = v.split('.');
    //integer will get !part[1]
    if(!part[1] || part[1] && part[1].length <= pos_trail_zero)
	{
        v = (+v).toFixed(pos_trail_zero + 1);
    }
    var szSep = m[0].split(Group); //look for separator
    m[0] = szSep.join(''); //join back without separator for counting the pos of any leading 0.

    var pos_lead_zero = m[0] && m[0].indexOf('0');
    if(pos_lead_zero > -1)
	{
        while(part[0].length < (m[0].length - pos_lead_zero))
		{
            part[0] = '0' + part[0];
        }
    }
    else if(+part[0] == 0)
	{
        part[0] = '';
    }

    v = v.split('.');
    v[0] = part[0];
    
    //process the first group separator from decimal (.) only, the rest ignore.
    //get the length of the last slice of split result.
    var pos_separator = (szSep[1] && szSep[szSep.length - 1].length);
    if(pos_separator)
	{
        var integer = v[0];
        var str = '';
        var offset = integer.length % pos_separator;
        for(var i = 0, l = integer.length; i < l; i++)
		{

            str += integer.charAt(i); //ie6 only support charAt for sz.
            //-pos_separator so that won't trail separator on full length
            if(!((i - offset + 1) % pos_separator) && i < l - pos_separator)
			{
                str += Group;
            }
        }
        v[0] = str;
    }

    v[1] = (m[1] && v[1]) ? Decimal + v[1] : "";
    return (isNegative ? '-' : '') + v[0] + v[1]; //put back any negation and combine integer and fraction.
}

interface Date
{
	now():number;
}

Date.now = function()
{
	return new Date().getTime();
}

class Matrix2D
{
	static identity = new Matrix2D().identity();
	matrix = new Float32Array([1,0,0,1,0,0]);
	
	identity()
	{
		var out = this.matrix;
		out[0] = 1;
		out[1] = 0;
		out[2] = 0;
		out[3] = 1;
		out[4] = 0;
		out[5] = 0;		
		return this;
	}
	
	set(source:Matrix2D)
	{
		var out = this.matrix, a=source.matrix;
		out[0] = a[0];
		out[1] = a[1];
		out[2] = a[2];
		out[3] = a[3];
		out[4] = a[4];
		out[5] = a[5];
	}
	
	get(index:number) { return this.matrix[index]; }
	get position() { return new Vector2(this.matrix[4], this.matrix[5]); }
	
	rotate(rad:number, source?:Matrix2D)
	{
		var a = source ? source.matrix : this.matrix, out = this.matrix;
		var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
		s = Math.sin(rad),
		c = Math.cos(rad);
		out[0] = a0 *  c + a2 * s;
		out[1] = a1 *  c + a3 * s;
		out[2] = a0 * -s + a2 * c;
		out[3] = a1 * -s + a3 * c;
		out[4] = a4;
		out[5] = a5;		
	}
	
	scale(v:Vector2, source?:Matrix2D)
	{
		var a = source ? source.matrix : this.matrix, out = this.matrix;
		var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
			v0 = v.x, v1 = v.y;
		out[0] = a0 * v0;
		out[1] = a1 * v0;
		out[2] = a2 * v1;
		out[3] = a3 * v1;
		out[4] = a4;
		out[5] = a5;		
	}
	
	translate(v:Vector2, source?:Matrix2D)
	{
		var a = source ? source.matrix : this.matrix, out = this.matrix;
		var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
		v0 = v.x, v1 = v.y;
		out[0] = a0;
		out[1] = a1;
		out[2] = a2;
		out[3] = a3;
		out[4] = a0 * v0 + a2 * v1 + a4;
		out[5] = a1 * v0 + a3 * v1 + a5;		
	}
	
	invert(source?:Matrix2D)
	{
		var a = source ? source.matrix : this.matrix, out = this.matrix;
		var aa = a[0], ab = a[1], ac = a[2], ad = a[3],
		atx = a[4], aty = a[5];
		
		var det = aa * ad - ab * ac;
		if(!det) return;
		det = 1.0 / det;
		
		out[0] = ad * det;
		out[1] = -ab * det;
		out[2] = -ac * det;
		out[3] = aa * det;
		out[4] = (ac * aty - ad * atx) * det;
		out[5] = (ab * atx - aa * aty) * det;		
	}
	
	get inverse()
	{
		var target = new Matrix2D();
		var a = this.matrix, out = target.matrix;
		var aa = a[0], ab = a[1], ac = a[2], ad = a[3],
		atx = a[4], aty = a[5];
		
		var det = aa * ad - ab * ac;
		if(!det) return;
		det = 1.0 / det;
		
		out[0] = ad * det;
		out[1] = -ab * det;
		out[2] = -ac * det;
		out[3] = aa * det;
		out[4] = (ac * aty - ad * atx) * det;
		out[5] = (ab * atx - aa * aty) * det;
		
		return target;
	}
	
	multiply(value1:Matrix2D, value2?:Matrix2D) 
	{
		var out = this.matrix;
		if(value2) {var a = value1.matrix, b = value2.matrix;}
		else       {var a = this.matrix, b = value1.matrix;}
		
		var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
			b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];
		out[0] = a0 * b0 + a2 * b1;
		out[1] = a1 * b0 + a3 * b1;
		out[2] = a0 * b2 + a2 * b3;
		out[3] = a1 * b2 + a3 * b3;
		out[4] = a0 * b4 + a2 * b5 + a4;
		out[5] = a1 * b4 + a3 * b5 + a5;
	}
	
	transformVertices(vertices:number[]) 
	{
		var m = this.matrix;
		var m0 = m[0], m1 = m[1], m2 = m[2], 
		    m3 = m[3], m4 = m[4], m5 = m[5];
		
		for(var i=0; i<vertices.length; i+=2)
		{
			var x = vertices[i];
			var y = vertices[i+1];
			vertices[i]   = m0*x + m2*y + m4;
			vertices[i+1] = m1*x + m3*y + m5;
		}
	}
}

class Rect
{
	constructor(public x1?:number, public y1?:number, public x2?:number, public y2?:number) {}
	
	isIntersecting(rect:Rect)
	{
		return rect.x1 <= this.x2 && rect.x2 >= this.x1 && rect.y1 <= this.y2 && rect.y2 >= this.y1;
	}
	
	contains(value:Vector2)
	{
		return value.x >= this.x1 && value.x < this.x2 && value.y >= this.y1 && value.y < this.y2;
	}
	
	relativeTo(matrix:Matrix2D)
	{
		var a = matrix.matrix;
		var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
		
		var det = aa * ad - ab * ac;
		if(!det) return;
		det = 1.0 / det;
		
		return new Rect(
			( ad * det) * this.x1 + (-ac * det) * this.y1 + ((ac * aty - ad * atx) * det),
			(-ab * det) * this.x1 + ( aa * det) * this.y1 + ((ab * atx - aa * aty) * det),
			( ad * det) * this.x2 + (-ac * det) * this.y2 + ((ac * aty - ad * atx) * det),
			(-ab * det) * this.x2 + ( aa * det) * this.y2 + ((ab * atx - aa * aty) * det)
		)
	}	
}

function limitedClone<T>(obj:T, depth = 3):T
{
    if(obj === null || typeof(obj) !== 'object')
        return obj;

    var clonedObject = <T>{};

    for(var key in obj)
	{
        if (obj.hasOwnProperty(key))
		{
            if(depth > 0) clonedObject[key] = limitedClone(obj[key], depth - 1);
            else          clonedObject[key] = obj[key];
        }
    }
    
    return clonedObject;
}

function clone<T>(obj:T):T
{
    if(obj === null || typeof(obj) !== 'object')
        return obj;

	if(obj instanceof Array)
	{
		var clonedArray = [];
		var length = (<any>obj).length;
		for(var i=0; i < length; i++)
			clonedArray[i] = clone(obj[i]);
	    return <any>clonedArray;
	}
	else
	{
		var clonedObject = <T>{};
		for(var key in obj)
			if (obj.hasOwnProperty(key))
				clonedObject[key] = clone(obj[key]);
	    return clonedObject;
	}
}

var epsilon = .00001;

function create2DArray<T>(x:number, y:number, value:T = null):T[][]
{
	var array = new Array<T[]>(x);
	for(var i=0; i<x; i++)
	{
		var row = new Array<T>(y);
		for(var j=0; j<y; j++)
			row[j] = value;
		array[i] = row;
	}
	return array;
}

/*
// HTTP using node.js
function http(method:string, url:string)
{
  // return new pending promise
  return new Promise((resolve, reject) => {
    // select http or https module, depending on reqested url
    var options = require('url').parse(url);
    options.method = method;
    const request = require('https').request(options, response => {
      // handle http errors
      if (response.statusCode < 200 || response.statusCode > 299) {
         reject(new Error('Failed to load page, status code: ' + response.statusCode));
       }
      // temporary data holder
      const body = [];
      // on every content chunk, push it to the data array
      response.on('data', (chunk) => body.push(chunk));
      // we are done, resolve promise with those joined chunks
      response.on('end', () => resolve(body.join('')));
    });
    // handle connection errors of the request
    request.on('error', (err) => reject(err))
    })
};
*/

function wait(milliseconds:number)
{
	return new Promise((resolve,reject) => {
		setTimeout(() => {
			resolve();
		}, milliseconds)
	})
}

function getLocalTimeString()
{
	var now = new Date();
	return `${now.getFullYear()}.${now.getMonth()+1}.${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`
}
