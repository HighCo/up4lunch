declare function require(value);
const node_http = require('http');
const fs = require('fs')
const mongodb = require('mongodb')
const mongoClient:MongoClient = mongodb.MongoClient
const ObjectID:{new(s?: string | number):ObjectID} = mongodb.ObjectID

var server, db:Db;
if(server) server.close();


mongoClient.connect("mongodb://localhost:27099")
.then(client => {
	db = client.db("up4lunch");
	
	server = node_http.createServer(function (request, response) {
		onRequest(request, response);
	});
	server.listen(3003)
	init();
	console.log("Up4Lunch server started.");
})


declare function _sendToEditor(data:any);

function log(value:string)
{
	if(typeof _sendToEditor != "undefined") _sendToEditor({type:"log", value:value})
	else                                    console.log(value);
}