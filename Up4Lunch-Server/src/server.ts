declare var __dirname;
var startTime = new Date();
var handlerMap:{[path:string]:(path,body?) => Promise<string>} = {}
var users:Collection;

function post(action, callback:(path,body?) => Promise<string>)
{
	handlerMap["POST"+action] = callback
}

function init()
{
	users = db.collection("users");
}

function onRequest(request, response)
{
	var body = "";
	request.on("data", a => body += a)
	request.on("end", async () => 
	{
		response.setHeader('Access-Control-Allow-Origin', '*');
		response.writeHead(200, {'Content-Type': 'text/plain'});
		var path = request.url.split("/");
		var handler = handlerMap[request.method+path[2]]
		if(handler)
		{
			var result = await handler(path.splice(0,3), body ? JSON.parse(body) : null);
			response.end(JSON.stringify(result));
		}
		else
			response.end("Up4lunch server up since "+startTime+", version 1");
	})
}

post("user", async (path, body) =>
{
	log("post user "+JSON.stringify(body))
	await users.save({
		_id:new ObjectID(body._id),
		name:body.name,
		email:body.email,
	})
	return "ok"
})
