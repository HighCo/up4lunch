// base.js
Array.prototype.clear = function () {
    while (this.length > 0)
        this.pop();
};
Array.prototype.add = function (value) {
    this.push(value);
    return value;
};
Array.prototype.ensure = function (value) {
    if (!this.contains(value))
        this.push(value);
    return value;
};
Array.prototype.any = function (condition) {
    for (var i = 0; i < this.length; i++)
        if (condition(this[i]))
            return true;
    return false;
};
Array.prototype.all = function (condition) {
    for (var i = 0; i < this.length; i++)
        if (!condition(this[i]))
            return false;
    return true;
};
Array.prototype.first = function (condition) {
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (condition(item))
            return item;
    }
    return null;
};
Array.prototype.last = function (condition) {
    for (var i = this.length - 1; i >= 0; i--) {
        var item = this[i];
        if (condition(item))
            return item;
    }
    return null;
};
Array.prototype.contains = function (item) {
    return this.indexOf(item) != -1;
};
Array.prototype.pushIfDistinct = function (item) {
    if (this.indexOf(item) != -1)
        this.push(item);
};
Array.prototype.removeWhere = function (condition) {
    for (var i = this.length - 1; i >= 0; i--) {
        var item = this[i];
        if (condition(item))
            this.splice(i, 1);
    }
};
Array.prototype.remove = function (item) {
    var index = this.indexOf(item);
    if (index > -1)
        this.splice(index, 1);
};
Array.prototype.removeAt = function (index) {
    this.splice(index, 1);
};
Array.prototype.minIndex = function (selector) {
    if (this.length > 0) {
        if (selector) {
            var minIndex = 0;
            var minValueAfterSelector = selector(this[0]);
            for (var i = 1; i < this.length; i++) {
                var valueAfterSelector = selector(this[i]);
                if (valueAfterSelector < minValueAfterSelector) {
                    minValueAfterSelector = valueAfterSelector;
                    minIndex = i;
                }
            }
            return minIndex;
        }
        else {
            var minIndex = 0;
            var minValue = this[0];
            for (var i = 1; i < this.length; i++) {
                var value = this[i];
                if (value < minValue) {
                    minValue = value;
                    minIndex = i;
                }
            }
            return minIndex;
        }
    }
    else
        return -1;
};
Array.prototype.maxIndex = function (selector) {
    if (this.length > 0) {
        if (selector) {
            var maxIndex = 0;
            var maxValueAfterSelector = selector(this[0]);
            for (var i = 1; i < this.length; i++) {
                var valueAfterSelector = selector(this[i]);
                if (valueAfterSelector > maxValueAfterSelector) {
                    maxValueAfterSelector = valueAfterSelector;
                    maxIndex = i;
                }
            }
            return maxIndex;
        }
        else {
            var maxIndex = 0;
            var minValue = this[0];
            for (var i = 1; i < this.length; i++) {
                var value = this[i];
                if (value > minValue) {
                    minValue = value;
                    maxIndex = i;
                }
            }
            return maxIndex;
        }
    }
    else
        return -1;
};
Array.prototype.min = function (selector, theshold) {
    if (this.length > 0) {
        if (selector) {
            if (theshold !== void 0) {
                var minValue = null;
                var minValueAfterSelector = theshold;
                var start = 0;
            }
            else {
                var minValue = this[0];
                var minValueAfterSelector = selector(minValue);
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                var valueAfterSelector = selector(value);
                if (valueAfterSelector < minValueAfterSelector) {
                    minValueAfterSelector = valueAfterSelector;
                    minValue = value;
                }
            }
            return minValue;
        }
        else {
            if (theshold !== void 0) {
                var minValue = theshold;
                var start = 0;
            }
            else {
                var minValue = this[0];
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                if (value < minValue) {
                    minValue = value;
                }
            }
            return minValue < theshold ? minValue : null;
        }
    }
    else
        return selector ? null : theshold;
};
Array.prototype.minValue = function (selector, theshold) {
    if (this.length > 0) {
        if (selector) {
            if (theshold !== void 0) {
                var minValue = null;
                var minValueAfterSelector = theshold;
                var start = 0;
            }
            else {
                var minValue = this[0];
                var minValueAfterSelector = selector(minValue);
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                var valueAfterSelector = selector(value);
                if (valueAfterSelector < minValueAfterSelector) {
                    minValueAfterSelector = valueAfterSelector;
                    minValue = value;
                }
            }
            return minValueAfterSelector;
        }
        else {
            if (theshold !== void 0) {
                var minValue = theshold;
                var start = 0;
            }
            else {
                var minValue = this[0];
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                if (value < minValue) {
                    minValue = value;
                }
            }
            return minValue < theshold ? minValue : null;
        }
    }
    else
        return theshold;
};
Array.prototype.max = function (selector, theshold) {
    if (this.length > 0) {
        if (selector) {
            if (theshold !== void 0) {
                var maxValue = null;
                var maxValueAfterSelector = theshold;
                var start = 0;
            }
            else {
                var maxValue = this[0];
                var maxValueAfterSelector = selector(maxValue);
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                var valueAfterSelector = selector(value);
                if (valueAfterSelector > maxValueAfterSelector) {
                    maxValueAfterSelector = valueAfterSelector;
                    maxValue = value;
                }
            }
            return maxValue;
        }
        else {
            if (theshold !== void 0) {
                var maxValue = theshold;
                var start = 0;
            }
            else {
                var maxValue = this[0];
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                if (value > maxValue) {
                    maxValue = value;
                }
            }
            return maxValue;
        }
    }
    else
        return selector ? null : theshold;
};
Array.prototype.maxValue = function (selector, theshold) {
    if (this.length > 0) {
        if (selector) {
            if (theshold !== void 0) {
                var maxValue = null;
                var maxValueAfterSelector = theshold;
                var start = 0;
            }
            else {
                var maxValue = this[0];
                var maxValueAfterSelector = selector(maxValue);
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                var valueAfterSelector = selector(value);
                if (valueAfterSelector > maxValueAfterSelector) {
                    maxValueAfterSelector = valueAfterSelector;
                    maxValue = value;
                }
            }
            return maxValueAfterSelector;
        }
        else {
            if (theshold !== void 0) {
                var maxValue = theshold;
                var start = 0;
            }
            else {
                var maxValue = this[0];
                var start = 1;
            }
            for (var i = start; i < this.length; i++) {
                var value = this[i];
                if (value > maxValue) {
                    maxValue = value;
                }
            }
            return maxValue;
        }
    }
    else
        return theshold;
};
Array.prototype.sum = function (selector) {
    if (this.length > 0) {
        if (selector) {
            var result = 0;
            for (var i = 0; i < this.length; i++)
                result += selector(this[i]);
            return result;
        }
        else {
            var result = 0;
            for (var i = 0; i < this.length; i++)
                result += this[i];
            return result;
        }
    }
    else
        return 0;
};
Array.prototype.insertAfter = function (position, value) {
    var pos = this.indexOf(position);
    if (pos > -1)
        this.splice(pos + 1, 0, value);
    else
        this.push(value);
};
Array.prototype.insertAt = function (index, value) {
    this.splice(index, 0, value);
};
Array.prototype.distinct = function (selector) {
    if (selector) {
        var existingValues = {}, result = [];
        for (var i = 0; i < this.length; i++) {
            var value = selector(this[i]);
            if (value === void 0 || existingValues[value] == true)
                continue;
            existingValues[value] = true;
            result.push(value);
        }
        return result;
    }
    else {
        var existingValues = {}, result = [];
        for (var i = 0; i < this.length; i++) {
            var value = this[i];
            if (value === void 0 || existingValues[value] == true)
                continue;
            existingValues[value] = true;
            result.push(value);
        }
        return result;
    }
};
Array.prototype.count = function (condition) {
    var count = 0;
    for (var _i = 0, _a = this; _i < _a.length; _i++) {
        var item = _a[_i];
        if (condition(item))
            count++;
    }
    return count;
};
Array.prototype.shuffle = function () {
    for (var i = this.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
};
Array.prototype.orderBy = function (selector) {
    return this.sort(function (a, b) { return selector(a) - selector(b); });
};
Array.prototype.orderByDescending = function (selector) {
    return this.sort(function (a, b) { return selector(b) - selector(a); });
};
Array.prototype.pushRange = function (values) {
    for (var _i = 0, values_1 = values; _i < values_1.length; _i++) {
        var item = values_1[_i];
        this.push(item);
    }
};
Array.prototype.set = function (values) {
    while (this.length > 0)
        this.pop();
    for (var _i = 0, values_2 = values; _i < values_2.length; _i++) {
        var item = values_2[_i];
        this.push(item);
    }
};
Array.prototype.random = function () {
    if (this.length > 0)
        return this[Math.floor(Math.random() * this.length)];
    else
        return -1;
};
Array.prototype.equals = function (otherArray) {
    if (!otherArray || this.length != otherArray.length)
        return false;
    for (var i = 0; i < this.length; i++)
        if (this[i] != otherArray[i])
            return false;
    return true;
};
String.prototype.startsWith = function (value) {
    return this.substring(0, value.length) == value;
};
String.prototype.startsWithAny = function (values) {
    for (var i = 0; i < values.length; i++) {
        var value = values[i];
        if (this.substring(0, value.length) == value)
            return true;
    }
    return false;
};
String.prototype.endsWith = function (value) {
    return this.substring(this.length - value.length) == value;
};
String.prototype.contains = function (value) {
    return this.indexOf(value) != -1;
};
String.prototype.containsAny = function (values) {
    for (var i = 0; i < values.length; i++) {
        var value = values[i];
        if (this.indexOf(value) != -1)
            return true;
    }
    return false;
};
String.prototype.trimLeft = function (chars) {
    if (chars === void 0) { chars = "\s"; }
    return this.replace(new RegExp("^[" + chars + "]+"), "");
};
String.prototype.trimRight = function (chars) {
    if (chars === void 0) { chars = "\s"; }
    return this.replace(new RegExp("[" + chars + "]+$"), "");
};
var escapeHtmlEntityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};
String.prototype.escapeHtml = function () {
    return this.replace(/[&<>"'\/]/g, function (s) { return escapeHtmlEntityMap[s]; });
};
Object.defineProperty(Array, "x", {
    get: function () { return this[0]; },
    set: function (value) { this[0] = value; },
    configurable: true,
    enumerable: false
});
Object.defineProperty(Array, "y", {
    get: function () { return this[1]; },
    set: function (value) { this[1] = value; },
    configurable: true,
    enumerable: false
});
var Vector2 = (function () {
    function Vector2(x, y) {
        if (typeof x === "number") {
            this.x = x;
            this.y = y;
        }
        else {
            this.x = x.x;
            this.y = x.y;
        }
    }
    Object.defineProperty(Vector2, "minValue", {
        get: function () { return new Vector2(Number.MIN_VALUE, Number.MIN_VALUE); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2, "maxValue", {
        get: function () { return new Vector2(Number.MAX_VALUE, Number.MAX_VALUE); },
        enumerable: true,
        configurable: true
    });
    Vector2.prototype.clone = function () { return new Vector2(this.x, this.y); };
    Vector2.prototype.add = function (value) { return new Vector2(this.x + value.x, this.y + value.y); };
    Vector2.prototype.increase = function (value) { this.x += value.x; this.y += value.y; };
    Vector2.prototype.subtract = function (value) { return new Vector2(this.x - value.x, this.y - value.y); };
    Vector2.prototype.multiply = function (value) {
        if (typeof value === "number")
            return new Vector2(this.x * value, this.y * value);
        else
            return new Vector2(this.x * value.x, this.y * value.y);
    };
    Vector2.prototype.divide = function (value) {
        if (typeof value === "number")
            return new Vector2(this.x / value, this.y / value);
        else
            return new Vector2(this.x / value.x, this.y / value.y);
    };
    Vector2.prototype.equal = function (value) { return this.x == value.x && this.y == value.y; };
    Vector2.prototype.unequal = function (value) { return this.x != value.x || this.y != value.y; };
    Vector2.prototype.greaterOrEqual = function (value) { return this.x >= value.x && this.y >= value.y; };
    Vector2.prototype.greater = function (value) { return this.x > value.x && this.y > value.y; };
    Vector2.prototype.lessOrEqual = function (value) { return this.x <= value.x && this.y <= value.y; };
    Vector2.prototype.less = function (value) { return this.x < value.x && this.y < value.y; };
    Object.defineProperty(Vector2.prototype, "length", {
        get: function () { return Math.sqrt(this.x * this.x + this.y * this.y); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "inverse", {
        get: function () { return new Vector2(-this.x, -this.y); },
        enumerable: true,
        configurable: true
    });
    Vector2.prototype.distanceTo = function (value) {
        var x = this.x - value.x;
        var y = this.y - value.y;
        return Math.sqrt(x * x + y * y);
    };
    Vector2.prototype.transform = function (matrix) {
        var m = matrix.matrix;
        return new Vector2(m[0] * this.x + m[2] * this.y + m[4], m[1] * this.x + m[3] * this.y + m[5]);
    };
    Vector2.prototype.relativeTo = function (matrix) {
        var a = matrix.matrix;
        var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
        var det = aa * ad - ab * ac;
        if (!det)
            return;
        det = 1.0 / det;
        return new Vector2((ad * det) * this.x + (-ac * det) * this.y + ((ac * aty - ad * atx) * det), (-ab * det) * this.x + (aa * det) * this.y + ((ab * atx - aa * aty) * det));
    };
    Vector2.distanceBetween = function (a, b) {
        var x = a.x - b.x;
        var y = a.y - b.y;
        return Math.sqrt(x * x + y * y);
    };
    Vector2.angleBetween = function (a, b) {
        return Math.atan2(b.y - a.y, b.x - a.x);
    };
    Vector2.lerp = function (a, b, value) {
        var ivalue = 1 - value;
        return new Vector2(a.x * ivalue + b.x * value, a.y * ivalue + b.y * value);
    };
    Vector2.prototype.isInsidePoygon = function (poly) {
        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= this.y && this.y < poly[j].y) || (poly[j].y <= this.y && this.y < poly[i].y))
                && (this.x < (poly[j].x - poly[i].x) * (this.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
                && (c = !c);
        return c;
    };
    Vector2.prototype.normalize = function () {
        var length = Math.sqrt(this.x * this.x + this.y * this.y);
        return length != 0 ? new Vector2(this.x / length, this.y / length) : new Vector2(0, 0);
    };
    return Vector2;
}());
Math.limitToRange = function (value, min, max) {
    if (value < min)
        return min;
    if (value > max)
        return max;
    return value;
};
Number.prototype.limitToRange = function (min, max) {
    if (this < min)
        return min;
    if (this > max)
        return max;
    return this;
};
Number.prototype.limitToAbove = function (min) {
    if (this < min)
        return min;
    return this;
};
Number.prototype.limitToBelow = function (max) {
    if (this > max)
        return max;
    return this;
};
Number.prototype.format = function (format) {
    var m = format;
    var v = this;
    if (!m || isNaN(+v)) {
        return v; //return as it is.
    }
    //convert any string to number according to formation sign.
    var v = (m.charAt(0) == '-' ? -v : +v);
    var isNegative = v < 0 ? v = -v : 0; //process only abs(), and turn on flag.
    //search for separator for grp & decimal, anything not digit, not +/- sign, not #.
    var result = m.match(/[^\d\-\+#]/g);
    var Decimal = (result && result[result.length - 1]) || '.'; //treat the right most symbol as decimal 
    var Group = (result && result[1] && result[0]) || ','; //treat the left most symbol as group separator
    //split the decimal for the format string if any.
    var m = m.split(Decimal);
    //Fix the decimal first, toFixed will auto fill trailing zero.
    v = v.toFixed(m[1] && m[1].length);
    v = +(v) + ''; //convert number to string to trim off *all* trailing decimal zero(es)
    //fill back any trailing zero according to format
    var pos_trail_zero = m[1] && m[1].lastIndexOf('0'); //look for last zero in format
    var part = v.split('.');
    //integer will get !part[1]
    if (!part[1] || part[1] && part[1].length <= pos_trail_zero) {
        v = (+v).toFixed(pos_trail_zero + 1);
    }
    var szSep = m[0].split(Group); //look for separator
    m[0] = szSep.join(''); //join back without separator for counting the pos of any leading 0.
    var pos_lead_zero = m[0] && m[0].indexOf('0');
    if (pos_lead_zero > -1) {
        while (part[0].length < (m[0].length - pos_lead_zero)) {
            part[0] = '0' + part[0];
        }
    }
    else if (+part[0] == 0) {
        part[0] = '';
    }
    v = v.split('.');
    v[0] = part[0];
    //process the first group separator from decimal (.) only, the rest ignore.
    //get the length of the last slice of split result.
    var pos_separator = (szSep[1] && szSep[szSep.length - 1].length);
    if (pos_separator) {
        var integer = v[0];
        var str = '';
        var offset = integer.length % pos_separator;
        for (var i = 0, l = integer.length; i < l; i++) {
            str += integer.charAt(i); //ie6 only support charAt for sz.
            //-pos_separator so that won't trail separator on full length
            if (!((i - offset + 1) % pos_separator) && i < l - pos_separator) {
                str += Group;
            }
        }
        v[0] = str;
    }
    v[1] = (m[1] && v[1]) ? Decimal + v[1] : "";
    return (isNegative ? '-' : '') + v[0] + v[1]; //put back any negation and combine integer and fraction.
};
Date.now = function () {
    return new Date().getTime();
};
var Matrix2D = (function () {
    function Matrix2D() {
        this.matrix = new Float32Array([1, 0, 0, 1, 0, 0]);
    }
    Matrix2D.prototype.identity = function () {
        var out = this.matrix;
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 1;
        out[4] = 0;
        out[5] = 0;
        return this;
    };
    Matrix2D.prototype.set = function (source) {
        var out = this.matrix, a = source.matrix;
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[4] = a[4];
        out[5] = a[5];
    };
    Matrix2D.prototype.get = function (index) { return this.matrix[index]; };
    Object.defineProperty(Matrix2D.prototype, "position", {
        get: function () { return new Vector2(this.matrix[4], this.matrix[5]); },
        enumerable: true,
        configurable: true
    });
    Matrix2D.prototype.rotate = function (rad, source) {
        var a = source ? source.matrix : this.matrix, out = this.matrix;
        var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], s = Math.sin(rad), c = Math.cos(rad);
        out[0] = a0 * c + a2 * s;
        out[1] = a1 * c + a3 * s;
        out[2] = a0 * -s + a2 * c;
        out[3] = a1 * -s + a3 * c;
        out[4] = a4;
        out[5] = a5;
    };
    Matrix2D.prototype.scale = function (v, source) {
        var a = source ? source.matrix : this.matrix, out = this.matrix;
        var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], v0 = v.x, v1 = v.y;
        out[0] = a0 * v0;
        out[1] = a1 * v0;
        out[2] = a2 * v1;
        out[3] = a3 * v1;
        out[4] = a4;
        out[5] = a5;
    };
    Matrix2D.prototype.translate = function (v, source) {
        var a = source ? source.matrix : this.matrix, out = this.matrix;
        var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], v0 = v.x, v1 = v.y;
        out[0] = a0;
        out[1] = a1;
        out[2] = a2;
        out[3] = a3;
        out[4] = a0 * v0 + a2 * v1 + a4;
        out[5] = a1 * v0 + a3 * v1 + a5;
    };
    Matrix2D.prototype.invert = function (source) {
        var a = source ? source.matrix : this.matrix, out = this.matrix;
        var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
        var det = aa * ad - ab * ac;
        if (!det)
            return;
        det = 1.0 / det;
        out[0] = ad * det;
        out[1] = -ab * det;
        out[2] = -ac * det;
        out[3] = aa * det;
        out[4] = (ac * aty - ad * atx) * det;
        out[5] = (ab * atx - aa * aty) * det;
    };
    Object.defineProperty(Matrix2D.prototype, "inverse", {
        get: function () {
            var target = new Matrix2D();
            var a = this.matrix, out = target.matrix;
            var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
            var det = aa * ad - ab * ac;
            if (!det)
                return;
            det = 1.0 / det;
            out[0] = ad * det;
            out[1] = -ab * det;
            out[2] = -ac * det;
            out[3] = aa * det;
            out[4] = (ac * aty - ad * atx) * det;
            out[5] = (ab * atx - aa * aty) * det;
            return target;
        },
        enumerable: true,
        configurable: true
    });
    Matrix2D.prototype.multiply = function (value1, value2) {
        var out = this.matrix;
        if (value2) {
            var a = value1.matrix, b = value2.matrix;
        }
        else {
            var a = this.matrix, b = value1.matrix;
        }
        var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];
        out[0] = a0 * b0 + a2 * b1;
        out[1] = a1 * b0 + a3 * b1;
        out[2] = a0 * b2 + a2 * b3;
        out[3] = a1 * b2 + a3 * b3;
        out[4] = a0 * b4 + a2 * b5 + a4;
        out[5] = a1 * b4 + a3 * b5 + a5;
    };
    Matrix2D.prototype.transformVertices = function (vertices) {
        var m = this.matrix;
        var m0 = m[0], m1 = m[1], m2 = m[2], m3 = m[3], m4 = m[4], m5 = m[5];
        for (var i = 0; i < vertices.length; i += 2) {
            var x = vertices[i];
            var y = vertices[i + 1];
            vertices[i] = m0 * x + m2 * y + m4;
            vertices[i + 1] = m1 * x + m3 * y + m5;
        }
    };
    return Matrix2D;
}());
Matrix2D.identity = new Matrix2D().identity();
var Rect = (function () {
    function Rect(x1, y1, x2, y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    Rect.prototype.isIntersecting = function (rect) {
        return rect.x1 <= this.x2 && rect.x2 >= this.x1 && rect.y1 <= this.y2 && rect.y2 >= this.y1;
    };
    Rect.prototype.contains = function (value) {
        return value.x >= this.x1 && value.x < this.x2 && value.y >= this.y1 && value.y < this.y2;
    };
    Rect.prototype.relativeTo = function (matrix) {
        var a = matrix.matrix;
        var aa = a[0], ab = a[1], ac = a[2], ad = a[3], atx = a[4], aty = a[5];
        var det = aa * ad - ab * ac;
        if (!det)
            return;
        det = 1.0 / det;
        return new Rect((ad * det) * this.x1 + (-ac * det) * this.y1 + ((ac * aty - ad * atx) * det), (-ab * det) * this.x1 + (aa * det) * this.y1 + ((ab * atx - aa * aty) * det), (ad * det) * this.x2 + (-ac * det) * this.y2 + ((ac * aty - ad * atx) * det), (-ab * det) * this.x2 + (aa * det) * this.y2 + ((ab * atx - aa * aty) * det));
    };
    return Rect;
}());
function limitedClone(obj, depth) {
    if (depth === void 0) { depth = 3; }
    if (obj === null || typeof (obj) !== 'object')
        return obj;
    var clonedObject = {};
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (depth > 0)
                clonedObject[key] = limitedClone(obj[key], depth - 1);
            else
                clonedObject[key] = obj[key];
        }
    }
    return clonedObject;
}
function clone(obj) {
    if (obj === null || typeof (obj) !== 'object')
        return obj;
    if (obj instanceof Array) {
        var clonedArray = [];
        var length = obj.length;
        for (var i = 0; i < length; i++)
            clonedArray[i] = clone(obj[i]);
        return clonedArray;
    }
    else {
        var clonedObject = {};
        for (var key in obj)
            if (obj.hasOwnProperty(key))
                clonedObject[key] = clone(obj[key]);
        return clonedObject;
    }
}
var epsilon = .00001;
function create2DArray(x, y, value) {
    if (value === void 0) { value = null; }
    var array = new Array(x);
    for (var i = 0; i < x; i++) {
        var row = new Array(y);
        for (var j = 0; j < y; j++)
            row[j] = value;
        array[i] = row;
    }
    return array;
}
/*
// HTTP using node.js
function http(method:string, url:string)
{
  // return new pending promise
  return new Promise((resolve, reject) => {
    // select http or https module, depending on reqested url
    var options = require('url').parse(url);
    options.method = method;
    const request = require('https').request(options, response => {
      // handle http errors
      if (response.statusCode < 200 || response.statusCode > 299) {
         reject(new Error('Failed to load page, status code: ' + response.statusCode));
       }
      // temporary data holder
      const body = [];
      // on every content chunk, push it to the data array
      response.on('data', (chunk) => body.push(chunk));
      // we are done, resolve promise with those joined chunks
      response.on('end', () => resolve(body.join('')));
    });
    // handle connection errors of the request
    request.on('error', (err) => reject(err))
    })
};
*/
function wait(milliseconds) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve();
        }, milliseconds);
    });
}
function getLocalTimeString() {
    var now = new Date();
    return now.getFullYear() + "." + (now.getMonth() + 1) + "." + now.getDate() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
}
//# sourceMappingURL=base.js.map

// engine.js
var node_http = require('http');
var fs = require('fs');
var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
var ObjectID = mongodb.ObjectID;
var server, db;
if (server)
    server.close();
mongoClient.connect("mongodb://localhost:27099")
    .then(function (client) {
    db = client.db("up4lunch");
    server = node_http.createServer(function (request, response) {
        onRequest(request, response);
    });
    server.listen(3003);
    init();
    console.log("Up4Lunch server started.");
});
function log(value) {
    if (typeof _sendToEditor != "undefined")
        _sendToEditor({ type: "log", value: value });
    else
        console.log(value);
}
//# sourceMappingURL=engine.js.map

// server.js
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var startTime = new Date();
var handlerMap = {};
var users;
function post(action, callback) {
    handlerMap["POST" + action] = callback;
}
function init() {
    users = db.collection("users");
}
function onRequest(request, response) {
    var _this = this;
    var body = "";
    request.on("data", function (a) { return body += a; });
    request.on("end", function () { return __awaiter(_this, void 0, void 0, function () {
        var path, handler, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    response.setHeader('Access-Control-Allow-Origin', '*');
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    path = request.url.split("/");
                    handler = handlerMap[request.method + path[2]];
                    if (!handler) return [3 /*break*/, 2];
                    return [4 /*yield*/, handler(path.splice(0, 3), body ? JSON.parse(body) : null)];
                case 1:
                    result = _a.sent();
                    response.end(JSON.stringify(result));
                    return [3 /*break*/, 3];
                case 2:
                    response.end("Up4lunch server up since " + startTime + ", version 1");
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    }); });
}
post("user", function (path, body) { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                log("post user " + JSON.stringify(body));
                return [4 /*yield*/, users.save({
                        _id: new ObjectID(body._id),
                        name: body.name,
                        email: body.email,
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/, "ok"];
        }
    });
}); });
//# sourceMappingURL=server.js.map

